#!/bin/bash
set -e
set -x

export AWS_PAGER=""


export BUILD_ID=$(git describe --exact-match --tags $(git log -n1 --pretty='%H') || git rev-parse HEAD)
export deploy_bucket='teamfortytwo.deploy'
export s3_bucket_folder='jenkins-server'


jenkins_domain='ci.enterpriseauth.net'
hosted_zone_id='Z078662476YYM8IWT2IQ'
private_hosted_zone_id='Z09052317C5896IJC3TD'
vpc_id='vpc-fceb5799'

# at least 1 subnet should be in the same availability zone where jenkins server located
# jenkins server in us-west-2a
alb_subnets='subnet-0e55c06b,subnet-f87ad18f'
certificate_arn='arn:aws:acm:us-west-2:439838887486:certificate/f6eae289-0649-4526-ad5a-91c7d0d748af'


# bash issue-cert.sh Z078662476YYM8IWT2IQ ci.enterpriseauth.net



# volume with prod jenkins_home
export volume_id="vol-093a7aaa05b988307"
export volume_gpt_id="7a7b87e9-5d3a-45d3-b164-252f83ca1b87"

export DOCKER_IMAGE_VERSION=$BUILD_ID

jenkins_server_stack_name="jenkins-server"

jenkins_alb_stack_name="jenkins-alb"
jenkins_cert_stack_name="jenkins-certificate"



# Deploy Application Load Balancer
aws cloudformation deploy \
    --region us-west-2 \
    --capabilities CAPABILITY_IAM CAPABILITY_NAMED_IAM \
    --stack-name $jenkins_alb_stack_name \
    --template $(pwd)/jenkins-alb.yaml \
    --parameter-overrides \
        Subnets="$alb_subnets" \
        VpcID="$vpc_id" \
        JenkinsDomain="$jenkins_domain" \
        CertificateArn="$certificate_arn"


# Get Target group for load balancer
targetGroup=$(
    aws cloudformation describe-stack-resource \
        --stack-name $jenkins_alb_stack_name \
        --region us-west-2 \
        --logical-resource-id "JenkinsTargetGroup" \
        --output text --query "StackResourceDetail.PhysicalResourceId"
)


bash ./pack.sh


aws s3 cp $BUILD_ID.tgz s3://${deploy_bucket}/${s3_bucket_folder}/

sec_group_name='jenkins-vpc-access'
sec_group_id='sg-06dde2ce2e53648ce'
jenkins_subnet='subnet-071fc4d68031e1a44'




AMIID=$(aws ec2 describe-images \
    --owners amazon \
    --filters "Name=name,Values=amzn2-ami-hvm-2.0.*-x86_64-gp2" "Name=state,Values=available" \
    --query "reverse(sort_by(Images, &Name))[0].ImageId" \
    --output text)




# vpc-fceb5799 - prod vpc with erad jenkins_server_stack_name
# subnet-f87ad18f - subnet of vpc in us-west-2a availability zone
# Need to match availability zone to be able attach EBS volume
# Need to match vpc to be able communicate with ERAD servers that are located in VPC



aws cloudformation deploy \
  --capabilities CAPABILITY_NAMED_IAM \
  --region us-west-2 \
  --stack-name $jenkins_server_stack_name\
  --template-file  $(pwd)/jenkins-server.yaml \
  --parameter-overrides  \
    AMIID=${AMIID} \
    SshKeyName="shared_primary" \
    SecurityGroupId=${sec_group_id}\
    SubnetId=${jenkins_subnet}\
    RunnerIP="172.31.23.151"\
    DeployBucket=${deploy_bucket}\
    DeployFile="${s3_bucket_folder}/${BUILD_ID}.tgz"\
    VolumeId="${volume_id}"\
    VolumeGPTUUID=${volume_gpt_id}\
    ProdPass="${PROD_PASS}" \
    BuildID=${DOCKER_IMAGE_VERSION}\
    HostedZoneId="Z078662476YYM8IWT2IQ"\
    PrivateHostedZoneId="Z09052317C5896IJC3TD"\
    JenkinsDomain="$jenkins_domain"\
    # SecurityGroupName=${sec_group_name}\
    # LetsEncryptAccount="vplatunov@elevensoftware.com"\



# Get jenkins server instance id
instance=$(
    aws cloudformation describe-stack-resource \
        --stack-name $jenkins_server_stack_name \
        --region us-west-2 \
        --logical-resource-id "instance" \
        --output text --query "StackResourceDetail.PhysicalResourceId"
)

# Add Jenkins server to target group
aws elbv2 register-targets \
    --region us-west-2 \
    --target-group-arn $targetGroup \
    --targets "Id=$instance"

# Get Load balancer ARN and fetch DNSName
load_balancer_arn=$(
  aws cloudformation describe-stack-resource \
      --stack-name $jenkins_alb_stack_name \
      --region us-west-2 \
      --logical-resource-id JenkinsLoadBalancer \
      --output text \
      --query "StackResourceDetail.PhysicalResourceId"
)

alb_dns=$(
  aws elbv2 describe-load-balancers \
    --load-balancer-arns $load_balancer_arn \
    --output text \
    --query "LoadBalancers[0].DNSName"
)

alb_hosted_zone=$(
  aws elbv2 describe-load-balancers \
    --load-balancer-arns $load_balancer_arn \
    --output text \
    --query "LoadBalancers[0].CanonicalHostedZoneId"
)

# Create Alias record to map our domain to Load balancer domain
cat << EOF > jenkins_dns_record.json
{
            "Comment": "UPSERT a Alias record ",
            "Changes": [{
            "Action": "UPSERT",
                        "ResourceRecordSet": {
                        "Name": "$jenkins_domain.",
                        "Type": "A",
                        "AliasTarget": {
                                "HostedZoneId": "$alb_hosted_zone",
                                "DNSName": "dualstack.$alb_dns.",
                                "EvaluateTargetHealth": true
                        }
                    }
            }]
}
EOF

aws route53 change-resource-record-sets \
  --hosted-zone-id "$hosted_zone_id" \
  --change-batch file://jenkins_dns_record.json


# Create the same record in private enterpriseauth.net Route53 hosted zone

aws route53 change-resource-record-sets \
  --hosted-zone-id "$private_hosted_zone_id" \
  --change-batch file://jenkins_dns_record.json




echo "Deploy complete!"