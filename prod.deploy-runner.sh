#!/bin/bash
set -x


AMIID=$(aws ec2 describe-images \
    --owners amazon \
    --filters "Name=name,Values=amzn2-ami-hvm-2.0.*-x86_64-gp2" "Name=state,Values=available" \
    --query "reverse(sort_by(Images, &Name))[0].ImageId" \
    --output text)

sec_group_id='sg-06dde2ce2e53648ce'
subnet_id='subnet-071fc4d68031e1a44'

runner_stack_name="jenkins-runner-$(date -u +%Y-%m-%d-%H-%M%-SZ)"




aws cloudformation deploy \
  --capabilities CAPABILITY_NAMED_IAM \
  --region us-west-2 \
  --stack-name $runner_stack_name \
  --template-file  $(pwd)/jenkins-runner.yaml \
  --parameter-overrides  \
    AMIID=${AMIID} \
    SshKeyName="prod-jenkins-runner" \
    SecurityGroupId=${sec_group_id}\
    SubnetId=${subnet_id}\


runner_ip=$(aws cloudformation describe-stacks \
    --stack-name $runner_stack_name \
    --query 'Stacks[0].Outputs[?OutputKey==`PrivateIp`].OutputValue' \
    --output text)


echo $runner_ip