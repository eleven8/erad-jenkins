#!/bin/bash
set -x

make_jobs() {
	sourc_dir=$1
    for f in ${sourc_dir}/*.xml
    do
        echo "Processing $f file..."
        job="${f##*/}"
        job="${job%%.*}"
        mkdir -p $dst/jobs/$job
        cp -f $f $dst/jobs/$job/config.xml
        chown jenkins:jenkins -R $dst/jobs/$job
        
    done

}


export dst=$JENKINS_HOME
export jobs_source=$JENKINS_JOBS 

make_jobs $jobs_source
