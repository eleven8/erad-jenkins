#!/bin/bash

dst=$(mktemp -d)


export BUILD_ID=$(git describe --exact-match --tags $(git log -n1 --pretty='%H') || git rev-parse HEAD)

tar -vczf $dst/${BUILD_ID}.tgz \
	casc\
	jobs\
	backup_history.sh\
	Dockerfile\
	install_jobs.sh\
	prod.docker-compose.yaml\
	prod.env.asc\
	restore_history.sh

mv $dst/${BUILD_ID}.tgz  .