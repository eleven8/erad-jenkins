#!/bin/bash
set -e
set -x


stack_name="jenkins-backup"

aws cloudformation deploy \
  --capabilities CAPABILITY_NAMED_IAM \
  --region us-west-2 \
  --stack-name $stack_name\
  --template-file  $(pwd)/jenkins-backup.yaml \
