FROM jenkins/jenkins:2.349-jdk11

ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false

COPY plugins.txt /usr/share/jenkins/ref/
RUN \
	/bin/jenkins-plugin-cli --plugin-file "/usr/share/jenkins/ref/plugins.txt" --verbose

ENV CASC_JENKINS_CONFIG /etc/jenkins_casc/casc.yaml

COPY install_jobs.sh /usr/local/bin/
COPY backup_history.sh /usr/local/bin/
COPY restore_history.sh /usr/local/bin/
COPY jobs /usr/share/jenkins/ref


# COPY custom.groovy /usr/share/jenkins/ref/init.groovy.d/custom.groovy