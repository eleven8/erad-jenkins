#!/bin/bash

save_history() {

    src_dir=$1
    dst_dir=$2
    HISTORY_ARCHIVE=$3


    touch $dst_dir/$HISTORY_ARCHIVE
    tar -vczf $dst_dir/$HISTORY_ARCHIVE \
        -C $src_dir \
        --exclude="jobs/*/config.xml" \
        --exclude="jobs/*/workspace*" \
        jobs
}


date_now=$(date "+%F_%H-%M-%S")
save_history $JENKINS_HOME $HISTORY_BACKUP_DIR "${date_now}_jenkins_history.tgz"

