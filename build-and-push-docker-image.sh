#!/bin/bash

export AWS_PAGER=""

export AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query 'Account' --output text)

echo $(aws ecr get-login-password --region us-west-2) | docker login --password-stdin --username AWS $AWS_ACCOUNT_ID.dkr.ecr.us-west-2.amazonaws.com

export BUILD_ID=$(git rev-parse HEAD)



docker-compose -f prod.docker-compose.yaml build jenkins
docker-compose -f prod.docker-compose.yaml push jenkins

echo "${AWS_ACCOUNT_ID}.dkr.ecr.us-west-2.amazonaws.com/jenkins-server:${BUILD_ID}"