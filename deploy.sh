#!/bin/bash
set -e
set -x


export BUILD_ID=$(git describe --exact-match --tags $(git log -n1 --pretty='%H') || git rev-parse HEAD)

deploy_bucket='iledum-jenkins'
hosted_zone_id='Z0118351BER6IANZ1EDU'
sec_group_name='jenkins-vpc-access'

jenkins_domain='ci.gold12.us'
vpc_id='vpc-65cee91d'
alb_subnets='subnet-ea489aa0,subnet-47b8083f' # At least two subnets should be specified.
certificate_arn='arn:aws:acm:us-west-2:228702658483:certificate/79c1c3ff-4e2a-46c1-b88a-7790a618f6cc'

export DOCKER_IMAGE_VERSION=$BUILD_ID


bash ./pack.sh


aws s3 cp $BUILD_ID.tgz s3://iledum-jenkins/


AMIID=$(aws ec2 describe-images \
    --owners amazon \
    --filters "Name=name,Values=amzn2-ami-hvm-2.0.*-x86_64-gp2" "Name=state,Values=available" \
    --query "reverse(sort_by(Images, &Name))[0].ImageId" \
    --output text)


jenkins_server_stack_name="jenkins-server-$(date -u +%Y-%m-%d-%H-%M%-SZ)"
jenkins_alb_stack_name="jenkins-alb-$(date -u +%Y-%m-%d-%H-%M%-SZ)"
jenkins_cert_stack_name="jenkins-certificate"

# Deploy Application Load Balancer
aws cloudformation deploy \
    --region us-west-2 \
    --capabilities CAPABILITY_IAM CAPABILITY_NAMED_IAM \
    --stack-name $jenkins_alb_stack_name \
    --template $(pwd)/jenkins-alb.yaml \
    --parameter-overrides \
        Subnets="$alb_subnets" \
        VpcID="$vpc_id" \
        JenkinsDomain="$jenkins_domain" \
        CertificateArn="$certificate_arn"

# Deploy Jenkins server
aws cloudformation deploy \
  --capabilities CAPABILITY_NAMED_IAM \
  --region us-west-2 \
  --stack-name $jenkins_server_stack_name\
  --template-file  $(pwd)/jenkins-server.yaml \
  --parameter-overrides  \
    AMIID=${AMIID} \
    SshKeyName="vpc-key" \
    SecurityGroupName=${sec_group_name}\
    RunnerIP="34.221.79.89"\
    DeployBucket=${deploy_bucket}\
    DeployFile=$BUILD_ID.tgz\
    VolumeId="vol-02c7fa7580ae092db"\
    VolumeGPTUUID="afe3ad16-571a-452f-8242-eda7e76cf8c6"\
    ProdPass="fake" \
    BuildID=${DOCKER_IMAGE_VERSION}\
    HostedZoneId="$hosted_zone_id"\
    JenkinsDomain="$jenkins_domain"\

# Get Target group for load balancer
targetGroup=$(
    aws cloudformation describe-stack-resource \
        --stack-name $jenkins_alb_stack_name \
        --region us-west-2 \
        --logical-resource-id "EradTargetGroup" \
        --output text --query "StackResourceDetail.PhysicalResourceId"
)

# Get jenkins server instance id
instance=$(
    aws cloudformation describe-stack-resource \
        --stack-name $jenkins_server_stack_name \
        --region us-west-2 \
        --logical-resource-id "instance" \
        --output text --query "StackResourceDetail.PhysicalResourceId"
)

# Add Jenkins server to target group
aws elbv2 register-targets \
    --region us-west-2 \
    --target-group-arn $targetGroup \
    --targets "Id=$instance"

# Get Load balancer ARN and fetch DNSName
load_balancer_arn=$(
  aws cloudformation describe-stack-resource \
      --stack-name $jenkins_alb_stack_name \
      --region us-west-2 \
      --logical-resource-id EradLoadBalancer \
      --output text \
      --query "StackResourceDetail.PhysicalResourceId"
)

alb_dns=$(
  aws elbv2 describe-load-balancers \
    --load-balancer-arns $load_balancer_arn \
    --output text \
    --query "LoadBalancers[0].DNSName"
)

# Create CNAME record to map our domain to Load balancer domain
cat << EOF > jenkins_dns_record.json
{
            "Comment": "UPSERT a CNAME record ",
            "Changes": [{
            "Action": "UPSERT",
                        "ResourceRecordSet": {
                        "Name": "$jenkins_domain",
                        "Type": "CNAME",
                        "TTL": 60,
                        "ResourceRecords": [
                            {
                                "Value": "$alb_dns"
                            }
                        ]
                    }
            }]
}
EOF

aws route53 change-resource-record-sets \
  --hosted-zone-id "$hosted_zone_id" \
  --change-batch file://jenkins_dns_record.json

echo "Deploy complete!"
