#!/bin/bash

set -e

if [ $# -ne 2 ]; then
	echo "Usage: $0 <route53_zone_id> <domain_name>"
	echo "example: $0 ZVMUXA2RIR4UY 'ci.gold2.us'"
	exit 1
fi

ZONE_ID=$1 #ZVMUXA2RIR4UY
DOMAIN_NAME=$2 #ci.gold12.us

# does not matter much, and valid only for 1 hour,
# after 1 hour AWS will issue a new cert even if idempotency_token is the same
IDEMPOTENCY_TOKEN='_ci_gold12_us'

# Certs for API Gateway have to be issued in the us-east-1 for edge optimized deployment
# https://docs.aws.amazon.com/acm/latest/userguide/acm-regions.html

REGION=us-west-2

cert_arn=$(aws acm request-certificate\
	--domain-name "$DOMAIN_NAME" \
	--validation-method DNS \
	--idempotency-token "$IDEMPOTENCY_TOKEN" \
	--region "$REGION" --output text)


# wait for certificate to be issued
echo waiting 15 seconds for certificate to be issued
sleep 15


read cname value <<< $(
aws acm describe-certificate \
--region $REGION \
--certificate-arn $cert_arn \
--query 'Certificate.DomainValidationOptions[0].ResourceRecord.[Name, Value]' --output text)

echo $cname
echo $value


cat <<EOF > cname-record-to-verify-domain.json
{
            "Comment": "UPSERT a record ",
            "Changes": [{
            "Action": "UPSERT",
                        "ResourceRecordSet": {
                                    "Name": "$cname",
                                    "Type": "CNAME",
                                    "TTL": 300,
                                 "ResourceRecords": [{ "Value": "$value"}]
}}]
}

EOF


aws route53 change-resource-record-sets --hosted-zone-id $ZONE_ID --change-batch file://cname-record-to-verify-domain.json


echo $cert_arn
